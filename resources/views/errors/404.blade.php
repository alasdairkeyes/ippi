@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Not Found</div>

                <div class="panel-body">
                    Oops, we couldn't find this page.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
